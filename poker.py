#!/usr/bin/env python3

'''
TASK:
Make sure a string is a valid hand?
Check for a pair of sevens?
Check for any pair?
Check for 3 of a kind?
Check for a full house?

Valid hand: 0-9, A,K,Q,J, Maximum 5 cards (only 4 for each card)
'''

import os
import re

hand = "AAA77";

def discoverCardCombination(combination):

	#Check for a pair of sevens
	sevenPairCombinationResult = re.findall(r"^.*([7]).*\1.*$", combination)

	#Check for 3 of a kind
	threeKingCombinationResult = re.findall(r"^.*([AKQJ0-9]).*\1.*\1.*$", combination)

	#Check for any pair
	anyPairCombinationResult = re.findall(r"^.*([AKQJ0-9]).*\1.*$", combination)

	#Full House (Don't finish yet)
	#fullHouseCombinationResult = re.findall(r"((\\w)\\2\\2(\\w)\\3|(\\w)\\4(\\w)\\5\\5)#.*", combination)
	#print(fullHouseCombinationResult)

	if sevenPairCombinationResult:
		print("Woohoo! We have 7 pair!")
	if threeKingCombinationResult:
		print("Woohoo! We have 3 of kind!")
	if anyPairCombinationResult:
		print("Woohoo! We have any pair!")

def errorExit():

	print("Sorry your cards input is incorrect")

def validate():

	regExValidateLength = r"^[AKQJ0-9]{5}$";
	regExValidateCountofRepeat = r"([AKQJ0-9])\1{4}";

	validateResultLength = re.fullmatch(regExValidateLength,hand);
	validateResultCountofRepeat = re.findall(regExValidateCountofRepeat,hand);

	if validateResultLength and len(validateResultCountofRepeat) == 0:
		discoverCardCombination(hand)
	else:
		errorExit()

validate()